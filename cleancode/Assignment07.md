***Assignment #07 (2024.03.09)***

#### 🔖 오늘 읽은 범위 : 5장. 형식 맞추기

---

##### 😃 책에서 기억하고 싶은 내용을 써보세요.
- 전문 개발자의 일차적인 의무
    - **의사소통 >** 돌아가는 코드 
    - 코드 형식은 의사소통의 일환이다
    1. **적절한 행 길이를 유지하라**
        - *큰 파일보다 작은 파일이 이해하기 쉽다*
        - ***파일 당 200줄**을 넘지 않고도 커다란 시스템을 구축할 수 있다*
        - **신문 기사처럼 작성하라** 
            - 이름은 간단하면서도 설명이 가능하게 짓는다
            - 소스 파일 ***첫 부분*** 은 **고차원 개념과 알고리즘을 설명**한다
            - 아래로 내려갈수록 의도를 세세하게 묘사한다
            - *마지막* 에는 가장 저차원 함수와 세부 내역이 나온다
        - **개념은 빈 행으로 분리하라**
            - **일련의 행 묶음**은 **완결된 생각 하나**를 표현한다
            - 빈 행은 새로운 개념을 시작한다는 시각적 단서다
        - **수직 거리**
            - 세로 밀집도는 연관성을 의미한다
            - 서로 밀접한 코드 행은 세로로 가까이에 놓아야 함
            - 변수는 사용하는 위치에 최대한 가까이 선언한다 
                - 단, 인스턴스 변수는 클래스 맨 처음에 선언한다
            - A함수가 B함수를 호출한다면 (종속함수) 세로로 가까이 배치한다
        - **세로 순서**
            - 함수 호출 종속성은 아래 방향으로 유지 (고차원 → 저차원)
                - 호출되는 함수가 아래에 위치 
                    ```
                    private void funcB() {
                        funcA();
                    }

                    private void funcA() {

                    }
                    ```
    2. **가로 형식 맞추기**
        - ***짧은 행**이 바람직하다. (~**120**, 그 이상은 주의부족)*
        - **가로 공백과 밀집도**
            - 공백을 사용해 밀접한 개념과 느슨한 개념을 표현한다
                ```
                // 함수 호출 인자를 공백을 두어 분리 
                double determinent = determinent(a, b, c);

                // 우선순위 연산에 공백을 두지 않았고, 
                // 항 사이에 공백을 둠 
                return -b + Math.sqrt(determinent) / (2*a);
                ```
        - **가로 정렬** 
            - 불필요한 탭 사용은 의도와 다르게 다른 부분에 집중하도록 할 수 있음
        - **들여쓰기**
            - scope 범위로 이루어진 계층을 표현하기 위해 코드를 들여쓴다
            - **코드가 속하는 범위**를 시각적으로 표현한다
            - 들여쓰기 한 파일은 **구조가 한 눈에** 들어온다 
        - **가짜 범위**
            - 빈 while 또는 for문 피하기 
            - 불가피하다면 다음과 같이 새 행에 세미콜론 작성 
                ```
                while(dis.read(buf, 0, readBufferSize) != -1)
                ;
                ```
    ##### 🤔 오늘 읽은 소감은? 떠오르는 생각을 가볍게 적어보세요.
    - **좋은 소프트웨어 시스템**은 ***읽기 쉬운 문서*** 📄 로 이뤄진다는 사실을 기억하고 내 작업에 반영하는 노력을 기울여야겠다.
    - **밥 아저씨의 형식 규칙**
        ```
        variable 
        constructor
        public static method 
        public method
        private method 
        getter & setter 
        low-level method
        ```
    - 다소 긴 함수와 작고 귀여운 함수라고 지칭한 예문을 보고 충격을 받았다. 저자의 함수 길이에 대한 허들이 너무 높은 것이 아닌가... 🥲
        - 다소 긴 함수
            ```
            private static void readPreferences() {
                InputStream is = null;
                try {
                    is = new FileInputStream(getPreferencesFile());
                    setPreferences(new Properties(getPreferences()));
                    getPreferences().load(is);
                } catch(IOException e) {
                try {
                    if(is != null)
                        is.close();
                } catch(Exception e1) {
                }
                }
            }
            ```
        - 작고 귀여운 함수 
            ```
            public int countTestCases() {
                int count = 0;
                for(Test each : tests)
                    count += each.countTestCases();
                return count;
            }
            ```

##### 🔎 궁금한 내용이 있거나, 잘 이해되지 않는 내용이 있다면 적어보세요.
- 
- 