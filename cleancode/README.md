### 🗓 [노개북 "클린코드" 스케쥴](https://nomadcoders.co/faq/challenge/book-schedule-clean-code)

#### 1 주차
- 금 | [Assignment #01]
책 갖고계시죠?
✔️ 책 인증 URL

- 토 | [Assignment #02]
📚 추천사 ~ 1장. 깨끗한 코드
✔️ TIL

- 일 | [Assignment #03]
📚 2장. 의미있는 이름
✔️ TIL, 인증 미션

- 월,화 | [Assignment #04](https://gitlab.com/bonni-e/today-i-learned/-/blob/main/cleancode/Assignment04.md)
📚 3장. 함수
✔️ TIL

- 수,목 | [Assignment #05](https://gitlab.com/bonni-e/today-i-learned/-/blob/main/cleancode/Assignment05.md)
📚 4장. 주석
✔️ TIL, 최애틸 미션


---

#### 2 주차
- 금 | [Assignment #06](https://replit.com/@trial1bloom/Assignment06)
📚 복습
✔️ 실전 적용 미션
    - 참고 자료 : [JS로 읽는 클린코드(한글판)](https://github.com/qkraudghgh/clean-code-javascript-ko)

- 토 | [Assignment #07](https://gitlab.com/bonni-e/today-i-learned/-/blob/main/cleancode/Assignment07.md)
📚 5장. 형식 맞추기
✔️ TIL

- 일 | [Assignment #08](https://gitlab.com/bonni-e/today-i-learned/-/blob/main/cleancode/Assignment08.md)
📚 6장. 객체와 자료구조
✔️ TIL

- 월 | [Assignment #09]
📚 복습
✔️ 퀴즈 Quiz (1)

- 화,수 | [Assignment #10](https://gitlab.com/bonni-e/today-i-learned/-/blob/main/cleancode/Assignment10.md)
📚 7장. 오류처리
✔️ TIL, 인증 미션

- 목,금 | [Assignment #11](https://gitlab.com/bonni-e/today-i-learned/-/blob/main/cleancode/Assignment11.md)
📚 9장. 단위 테스트
✔️ TIL, 최애틸 미션


---

#### 3 주차
- 토 | [Assignment #12]
📚 복습
✔️ 퀴즈 Quiz (2)

- 일,월 | [Assignment #13]
📚 복습
✔️ 퀴즈 Quiz (2)

- 화,수 | [Assignment #14]
📚 복습
✔️ 퀴즈 Quiz (2)

- 목 | [Assignment #15]
📚 복습
✔️ 최종 미션


---
