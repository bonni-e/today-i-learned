***Assignment #02 (2024.12.31)***

#### 🔖 오늘 읽은 범위 :
2장. **GPT 2.3~2.5** (46p~82p)

---

##### ✔ 오늘 새로 알게된 부분이 있다면 간략히 설명해주세요.
- PyTorch
    - Facebook's AI Research Lab(**FAIR**)에서 개발한 **오픈 소스 딥러닝 라이브러리**
    - 주로 인공신경망(Deep Learning) 모델을 설계하고 학습시킬 때 사용됨
    - 구성 : `torch` `torch.nn` `torch.optim` `torch.autograd` `torch.utils.data` `torchvision`
        - `torch` : 텐서 연산과 자동 미분 기능 등을 제공
        - `torch.nn` : 신경망(**n**eural **n**etworks) 구축에 필요한 다양한 레이어와 매개변수 관리 기능 제공
        - `torch.nn.functional` : 주로 상태가 없는(stateless)
 함수들(활성화 함수 `ReLU` `Sigmoid` 손실 함수 `Cross Entropy Loss` 등)을 제공, 함수적 인터페이스를 통해 레이어의 작동을 구현할 때 활용
 
##### ✔ 언어 모델 기본 구조 실습 후 이해하기.
- 2.3 언어 모델 만들기
    - ![](https://gitlab.com/bonni-e/today-i-learned/-/raw/main/%EC%8B%A4%EC%A0%84%20LLM%20%ED%8C%8C%EC%9D%B8%ED%8A%9C%EB%8B%9D/images/2-3.png)
    -   ```
        import torch
        import torch.nn as nn
        from torch.nn import functional as F

        class semiGPT(nn.Module):
            def __init__(self, vocab_length):
                super().__init__()
                self.embedding_token_table = nn.Embedding(vocab_length, vocab_length)

            def forward(self, inputs, targets=None):
                logits = self.embedding_token_table(inputs)
                if targets is None:
                    loss = None
                else:
                    batch, seq_length, vocab_length = logits.shape
                    logits = logits.view(batch * seq_length, vocab_length)
                    targets = targets.view(batch*seq_length)
                    loss = F.cross_entropy(logits, targets)
                return logits, loss

            def generate(self, inputs, max_new_tokens):
                for _ in range(max_new_tokens):
                    logits, loss = self.forward(inputs)
                    logits = logits[:, -1, :]
                    print(logits.shape)
                    probs = F.softmax(logits, dim=-1)
                    next_inputs = torch.multinomial(probs, num_samples=1)
                    inputs = torch.cat((inputs, next_inputs), dim=1)
                return inputs

        model = semiGPT(ko_vocab_size)
        logits, loss = model(example_x, example_y)
        print(loss)

        token_decode(model.generate(torch.zeros((1,1),
                                                dtype=torch.long),
                                    max_new_tokens=10)[0].tolist())
        ```
    - `nn.Module`
        - PyTorch에서의 모든 신경망 모델의 기본 클래스
        - 딥러닝 모델을 설계하고 관리하기 위해 제공        
    - `nn.Module`을 상속받은 semiGPT 클래스 정의
    - `__init__` 생성자 파라미터로 언어 모델이 다룰 수 있는 단어의 총 개수를 받음
    - [`nn.Embedding`](https://pytorch.org/docs/stable/generated/torch.nn.Embedding.html#torch.nn.Embedding)은 단어를 벡터(정한 차원 크기기의 숫자)로 변환하는 테이블을 만듦, 첫 번째 파라미터인 `num_embeddings`는 임베딩을 수행할 수 있는 최대 인덱스 값임
    - `forword` 메소드는 **실제로 데이터가 모델을 통과(계산)하는 과정** (입력 토큰에 대한 임베딩을 조회한 후, 확률로 변환되기 전의 상대적인 원시 점수 값인 **로짓 logit을 반환**함) (생성된 임베딩 테이블에서 inputs인 **정수형 텐서 즉, 토큰에 대응하는 임베딩 벡터를 반환**한 것)
    - `torch.nn.functional.cross_entropy()`
        - **손실 계산(Loss)** 있어야 모델이 학습될 수 있음
        - 중요한 가정1) 전체 손실은 개별 샘플 손실의 합과 같다
        - 중요한 가정2) 각 샘플의 손실을 계산할 때 **신경망의 최종 출력값과 입력값만 사용**한다
        - **크로스 엔트로피 함수**가 이를 충족하며, 분류 문제에서 모델 성능을 측정하는 데 자주 사용됨
        - 모델이 예측한 확률 분포(임베딩)와 실제 레이블(target)분포 간의 차이를 계산 (**손실이 낮을수록 모델의 예측이 실제 레이블에 가까움**을 의미)
- 2.4 Optimizer 추가하기
    - ![](https://gitlab.com/bonni-e/today-i-learned/-/raw/main/%EC%8B%A4%EC%A0%84%20LLM%20%ED%8C%8C%EC%9D%B8%ED%8A%9C%EB%8B%9D/images/2-3-1.png?ref_type=heads)
    - ![](https://gitlab.com/bonni-e/today-i-learned/-/raw/main/%EC%8B%A4%EC%A0%84%20LLM%20%ED%8C%8C%EC%9D%B8%ED%8A%9C%EB%8B%9D/images/2-3-2.png?ref_type=heads)
    - 
        ```
        model = semiGPT(ko_vocab_size).to(device)
        optimizer = torch.optim.AdamW(model.parameters(), lr=learning_rate) 
        for step in range(max_iteration):
            if step % eval_interval == 0 :
                losses = compute_loss_metrics()
                print(f'step : {step}, train loss : {losses["train"]:.4f}, val loss : {losses["eval"]:.4f}')

            example_x, example_y = batch_function("train")
            logits, loss = model(example_x, example_y)
            optimizer.zero_grad(set_to_none=True)
            loss.backward()
            optimizer.step()

        inputs = torch.zeros((1,1), dtype=torch.long, device=device)
        print(token_decode(model.generate(inputs, max_new_tokens=100)[0].tolist()))
        ```
    - `torch.optim.AdamW()`
        - 옵티마이저는 손실을 최소화하기 위해 모델의 매개변수 조정 과정을 담당 (성능 개선)
- 2.5 셀프 어텐션 추가하기
    - ![](https://gitlab.com/bonni-e/today-i-learned/-/raw/main/%EC%8B%A4%EC%A0%84%20LLM%20%ED%8C%8C%EC%9D%B8%ED%8A%9C%EB%8B%9D/images/2-5.png)
    - 
        ```
        class Head(nn.Module):
            def __init__(self, head_size):
                super().__init__()
                self.key = nn.Linear(n_embed, head_size, bias=False)
                self.query = nn.Linear(n_embed, head_size, bias=False)
                self.value = nn.Linear(n_embed, head_size, bias=False)
                self.register_buffer("tril", torch.tril(torch.ones(block_size, block_size)))

            def forward(self, inputs):
                batch_size, sequence_length, embedding_dim = inputs.shape
                keys = self.key(inputs)
                queries = self.query(inputs)
                weights = queries @ keys.transpose(-2, -1) * (embedding_dim ** -0.5)
                weights = weights.masked_fill(
                    self.tril[:sequence_length, :sequence_length] == 0, float("-inf")
                    )
                weights = F.softmax(weights, dim=-1)
                values = self.value(inputs)
                output = weights @ values
                return output
        ```
    - **어텐션 메커니즘**
        - 문자나 단어 사이의 **관계를 파악**하고, 특정 정보의 **중요성을 인식**하는 메커니즘
    - **셀프 어텐션**
        - **입력 시퀀스(문장)내 모든 단어 간의 관계를 직접 분석하고 처리**
