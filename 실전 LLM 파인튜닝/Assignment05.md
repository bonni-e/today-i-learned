***Assignment #5 (2025.1.3)***

#### 🔖 오늘 읽은 범위 :
3장. **전체 파인튜닝 3.2** (119p~131p)

---

##### ✔ 오늘 새로 알게된 부분이 있다면 간략히 설명해주세요.
- 
    - 

##### ✔ 그림을 확실하게 이해하셨나요?
- Gemma 모델 구조 분석
    - ![](https://gitlab.com/bonni-e/today-i-learned/-/raw/main/%EC%8B%A4%EC%A0%84%20LLM%20%ED%8C%8C%EC%9D%B8%ED%8A%9C%EB%8B%9D/images/Gemma.png)

    - 특히 그림을 확인하면서 흐름을 파악하는 것을 잊지마세요!

    - 기존 GPT와 다른 점은?

    - Gemma와 Gemma2 모델 비교
- Llama3 모델 구조 분석

    - ![](https://gitlab.com/bonni-e/today-i-learned/-/raw/main/%EC%8B%A4%EC%A0%84%20LLM%20%ED%8C%8C%EC%9D%B8%ED%8A%9C%EB%8B%9D/images/Llama3.png)
    - 마찬가지로 그림을 통해 GPT와 Llama3의 구조를 비교하면서 눈에 익혀보아요.

    - Llama3 특징?
- GPT, Gemma, Llama3 모델 특징 비교