***Assignment #01 (2024.12.30)***

#### 🔖 오늘 읽은 범위 :
1장. **NLP 과거와 현재** (1p\~23p)<br>
2장. **GPT 2.1~2.2** (26p~45p)

---

##### ✔ 오늘 새로 알게된 부분이 있다면 간략히 설명해주세요.
- **NLP(Natural Language Processing)**
    - 자연어 처리
    - 단순한 규칙으로 시작 → 복잡한 언어 모델로 발전
- 인공지능 발전의 중요한 이정표가 된 논문 세 편
    - 앨런 튜링
        - [Computing Machinery and Ontelligence](https://courses.cs.umbc.edu/471/papers/turing.pdf)
        - **기계는 생각할 수 있는가?** 💡
        - 사고의 개념 : 철학 < 실용 (기계의 행동으로 평가 → **튜링 테스트**)
    - 프랭크 로젠블랫
        - [The Perception](https://www.ling.upenn.edu/courses/cogs501/Rosenblatt1958.pdf) **퍼셉트론**
        - 인간의 뇌세포인 뉴런을 모방한 간단한 **인공 신경망 모델**
        - **현대 인공신경망과 딥러닝의 기초**가 되어 많은 인공지능의 기반이 됨
        - 연결주의 접근법(**자발적 조직화**), 통계적인 방법 활용
        - 선형적 분리 (한계 : 비선형적 분류 문제 해결 불가 → 암흑기 도래)
    - 데이비드 루멜하트, 제프리 힌튼, 로널드 윌리엄스
        - [Learning Representations by Back-Propagating Errors](https://gwern.net/doc/ai/nn/1986-rumelhart-2.pdf)
        - 인공지능의 **암흑기를 종식**시킨 논문
        - 퍼셉트론의 한계를 극복 → **비선형 문제 해결 방법** 제시
        - **역전파(Backpropagation) 알고리즘**
        - ![](https://gitlab.com/bonni-e/today-i-learned/-/raw/main/%EC%8B%A4%EC%A0%84%20LLM%20%ED%8C%8C%EC%9D%B8%ED%8A%9C%EB%8B%9D/images/backpropagation.png)

##### ✔ 언어 모델 기본 구조 실습 후 이해하기.
- **GPT(Generative Pre-trained Transformer)**
    - 2.2 데이터 준비와 모델 구성
    - ![](https://gitlab.com/bonni-e/today-i-learned/-/raw/main/%EC%8B%A4%EC%A0%84%20LLM%20%ED%8C%8C%EC%9D%B8%ED%8A%9C%EB%8B%9D/images/2-2.png)
