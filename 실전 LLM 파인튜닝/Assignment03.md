***Assignment #03 (2025.1.1)***

#### 🔖 오늘 읽은 범위 :
2장. **GPT 2.6~2.8** (89p~103p)

---

##### ✔ 오늘 새로 알게된 부분이 있다면 간략히 설명해주세요.
- 단일 헤드 어텐션 → 마스크드 셀프 어텐션 → **멀티헤드 어텐션**
    - 여러 개의 어텐션 메커니즘을 **병렬 처리**
    - **하나의 데이터를** 여러 관점에서 동시에 처리 후 결합합
    - 헤드가 많을 수록 언어의 더 다양한 특징을 파악할 수 있음음
- **피드포워드(Feedforward)** 네트워크
    - 어텐션 메커니즘만으로는 데이터의 **복잡한 패턴 및 비선형적 관계**를 충분히 학습하기 어려움 (한계점)
    - **어텐션 메커니즘의 결과물을 추가로 처리하고 변환**함
        - 각 어텐션 블록 뒤에 배치
        - 각 시퀀스 위치마다 독립적으로 적용

##### ✔ 언어 모델 기본 구조 실습 후 이해하기.
- 2.6 멀티헤드 어텐션과 피드포워드
    - ![](https://gitlab.com/bonni-e/today-i-learned/-/raw/main/%EC%8B%A4%EC%A0%84%20LLM%20%ED%8C%8C%EC%9D%B8%ED%8A%9C%EB%8B%9D/images/2-6.png)
- 2.7 Blocks 만들기
    - ![](https://gitlab.com/bonni-e/today-i-learned/-/raw/main/%EC%8B%A4%EC%A0%84%20LLM%20%ED%8C%8C%EC%9D%B8%ED%8A%9C%EB%8B%9D/images/2-7.png)
    - <img src="https://gitlab.com/bonni-e/today-i-learned/-/raw/main/%EC%8B%A4%EC%A0%84%20LLM%20%ED%8C%8C%EC%9D%B8%ED%8A%9C%EB%8B%9D/images/blocks.png" width="300px">
    - **Block**
        - GPT 모델의 핵심 개념
        - 모델의 설계와 구현에 중요한 구조적 단위위
- 2.8 토크나이저 만들기
    - ![](https://gitlab.com/bonni-e/today-i-learned/-/raw/main/%EC%8B%A4%EC%A0%84%20LLM%20%ED%8C%8C%EC%9D%B8%ED%8A%9C%EB%8B%9D/images/2-8.png)
