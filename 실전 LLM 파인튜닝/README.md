### 스터디 커리큘럼

> #### 1주차
- 1일차
    - **[개발 환경 설치](https://gitlab.com/bonni-e/today-i-learned/-/blob/main/%EC%8B%A4%EC%A0%84%20LLM%20%ED%8C%8C%EC%9D%B8%ED%8A%9C%EB%8B%9D/Assignment01.md?ref_type=heads)**
- 2일차     
    - **[언어 모델 만들기](https://gitlab.com/bonni-e/today-i-learned/-/blob/main/%EC%8B%A4%EC%A0%84%20LLM%20%ED%8C%8C%EC%9D%B8%ED%8A%9C%EB%8B%9D/Assignment02.md?ref_type=heads)**
- 3일차     
    - **[멀티헤드 어텐션 & 피드포워드](https://gitlab.com/bonni-e/today-i-learned/-/blob/main/%EC%8B%A4%EC%A0%84%20LLM%20%ED%8C%8C%EC%9D%B8%ED%8A%9C%EB%8B%9D/Assignment03.md)**
- 4일차
    - **[전체 파인튜닝 데이터 준비](https://gitlab.com/bonni-e/today-i-learned/-/blob/main/%EC%8B%A4%EC%A0%84%20LLM%20%ED%8C%8C%EC%9D%B8%ED%8A%9C%EB%8B%9D/Assignment04.md)**
- 5일차
    - **[Gemma & Llama3 모델 분석](https://gitlab.com/bonni-e/today-i-learned/-/blob/main/%EC%8B%A4%EC%A0%84%20LLM%20%ED%8C%8C%EC%9D%B8%ED%8A%9C%EB%8B%9D/Assignment05.md)**
> #### 2. 주차
- 6일차
    - **GPU 병렬화 기법**
- 7일차     
    - **단일 GPU Gemma 파인튜닝 (1)**
- 8일차     
    - **단일 GPU Gemma 파인튜닝 (2)**
- 9일차
    - **단일 GPU Gemma 파인튜닝 (3)**
- 10일차
    - **다중 GPU Gemma 파인튜닝**
> #### 3. 주차
- 11일차
    - **효율적인 파라미터 튜닝**
- 12일차     
    - **LoRA 튜닝 실습**
- 13일차     
    - **양자화 및 QLoRA**
- 14일차
    - **QLoRA 튜닝 실습**
- 15일차
    - **vLLM 서빙**