### DAY 7 (2023.01.19)

**🔖 오늘 읽은 범위 : 에피소드 22 ~ 에피소드 25**

---
😃 **책에서 기억하고 싶은 내용을 써보세요.**

- 메모리 (memory)
    - 비휘발성 : 컴퓨터 하드 드라이브 등 
    - 휘발성 : RAM(Random Access Memory)
        - 프로그램의 함수, 변수 등이 저장됨 
        - ex) int[] arr = new int[4];
        - ![ram](img/array.png){: width="200"}
- 자료구조와 알고리즘
    - 개발자라면 이를 이해하고 자신의 코드에 적용할 수 있어야 함. (보다 효율적인 코드)
    - 작업 과정 : 코딩 → 디버깅 → **"코드 정리"** 👈🏻 이 때 필요하다!
    - **자료구조**   
        - 데이터를 보관(정리)하는 방식 
        - 배열(array) 등
    - **알고리즘** (algorithm) 
        - 컴퓨터에게 내리는 지시사항의 나열 
        - **검색 알고리즘**
            - 선형 검색 알고리즘 (linear search) : 기본 배열 (앞에서부터 검색 시작)
            - 이진 검색 알고리즘 (binary search) : 정렬된 배열 (중앙에서부터 검색 시작)
        - **시간 복잡도** = 작업 속도 
            - **[Big-O 표기법](https://blog.teclado.com/time-complexity-big-o-notation-python/)** : 알고리즘으로 작업을 완료할 때까지 걸리는 절차의 수 N을 표현하는 방식 
            - ![bigo](http://rapapa.net/wp/wp-content/uploads/2018/04/%E1%84%89%E1%85%B3%E1%84%8F%E1%85%B3%E1%84%85%E1%85%B5%E1%86%AB%E1%84%89%E1%85%A3%E1%86%BA-2018-04-13-%E1%84%8B%E1%85%A9%E1%84%92%E1%85%AE-6.48.30.png){: width="450"}
                - O(1) : 상수시간(constant time)
                - O(N) : 선형시간(linear time) (선형 검색 알고리즘의 시간 복잡도)
                - O(N²) : 이차시간(quadratic time, O(N^2)) (중첩 반복문)

🤔 **오늘 읽은 소감은? 떠오르는 생각을 가볍게 적어보세요**

```
너무 어려워서 공부하다가 처박아 둔 알고리즘과 자료구조 개념을 
노마드 쌤의 원고로 설명을 다시 보니 너무나 재미있다. 
왜 설레지🧐?! 다시 공부를 해보자.
```

🔎 **궁금한 내용이 있거나, 잘 이해되지 않는 내용이 있다면 적어보세요.**

- 
-
