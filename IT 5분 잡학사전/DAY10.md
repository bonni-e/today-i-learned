### DAY 10 (2023.01.22)

**🔖 오늘 읽은 범위 : 에피소드 30 ~ 에피소드 34**

---
😃 **책에서 기억하고 싶은 내용을 써보세요.**

- 레거시 (legacy) 시스템 : 오래 전에 개발된 시스템

- 코볼 (COBOL, Common business oriented language)
    - 과거 영어와 닮아 파이썬처럼 인기가 많았던 프로그래밍 언어 
    - 미국 은행 시스템의 43%가 코볼로 개발됨 
    - 미국 ATM 시스템의 95%가 코볼로 개발됨 
    - 문제점 : 커뮤니티 크기가 매우매우매우 적음
- 코볼 사태(?)를 통해 얻을 수 있는 교훈 
    - 프로그램은 책임 있게 만들어야 한다 (돌아가기만 하면 되는 것이 아님)
    - 프로그램은 끊임없이 관리해야 한다 (오랫동안 방치하면 안됨, 코드를 살아있는 생명체처럼 대해야 함)

- **SQL** (Structured query language)
    - 데이터베이스에게 '질문 또는 문의'를 하기 위해 구조를 갖춘 언어 
- DBMS (Database Management system, 데이터베이스관리시스템)
    - 데이터베이스는 창고
    - 창고 관리자 역할은 DBMS가 함 
    - SQL은 창고관리자와 소통하기 위한 언어 
    - SQL ←(DBMS)→ DB 
    - 종류 : MySQL, PostgreSQL, SQLite, Oracle, MariaDB 등 
- **ORM** (Oriented relational mapping)
    - SQL을 프로그래밍 언어로 쓸 수 있게 해주는 것 
    - SQL 번역기와 같은 역할을 해주지만, 만능은 아님으로 개발자도 SQL 기초는 공부해야함 
    - 종류 : 
        | 언어 | ORM |
        | ------ | ------ |
        | Python | Django ORM |
        | Node.js | Sequalize ORM |
        | Java | Hybernate, JPA |
- NoSQL
    - document DB
        - 데이터 테이블(열과 행)의 개념이 없음 
        - 데이터 형식이 매우 자유로움 (데이터마다 구성이 같을 필요가 없음)
        - ex) 
            - MongoDB는 데이터를 JSON 도큐먼트 타입으로 저장함 
    - key-value DB
        - 읽고 쓰는 속도가 엄청 빠름 
        - ex) 
            - 카산드라디비(CassandraDB) : 애플, 넷플릭스, 인스타그램, 우버 등에서 사용함 
            - 다이나모디비(DynamoDB) : 아마존이 만듦, 언어학습 애플리케이션 듀오링고(Duolingo)에서 사용함
    - graph DB 
        - 노드로 관계를 표현함 
        
- Git & Github
    - Git : 파일 이력 관리 프로그램 
    - Github : 파일과 깃으로 관리한 이력을 저장하고 공유하는 공간 (파일 클라우드 서비스)
- 버전 표기 방식 
    - SemVer (Semantic versioning specification, Sem Ver) : 숫자 3개로 표시하는 버전 표기 방식임 
        - ex) 4.0.5, 1.1.0, 2.1.0 ...
    - 첫번째 자리 숫자 : 프로그램에 큰 변화가 있을 때 바뀜 
    - 중간 자리 숫자 : 마이너한 업데이트가 있을 때 바뀜, 살짝 업그레이드 
    - 마지막 자리 숫자 : 패치 또는 버그의 수정을 의미 

🤔 **오늘 읽은 소감은? 떠오르는 생각을 가볍게 적어보세요**

```

```
🔎 **궁금한 내용이 있거나, 잘 이해되지 않는 내용이 있다면 적어보세요.**

- 
-
