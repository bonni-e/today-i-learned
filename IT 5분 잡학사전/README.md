### 🗓 [노개북 "IT 잡학사전" 스케쥴](https://nomadcoders.co/c/it-dictionary/lobby)

#### 1 주차
- 금 | [Assignment #01](https://gitlab.com/bonni-e/today-i-learned/-/blob/main/IT%205%EB%B6%84%20%EC%9E%A1%ED%95%99%EC%82%AC%EC%A0%84/DAY1.md)
책 갖고계시죠?
✔️ 책 인증 URL
- 토 | [Assignment #02](https://gitlab.com/bonni-e/today-i-learned/-/blob/main/IT%205%EB%B6%84%20%EC%9E%A1%ED%95%99%EC%82%AC%EC%A0%84/DAY2.md)
📚 에피소드 1 ~ 에피소드 5
✔️ TIL
- 일 | [Assignment #03](https://gitlab.com/bonni-e/today-i-learned/-/blob/main/IT%205%EB%B6%84%20%EC%9E%A1%ED%95%99%EC%82%AC%EC%A0%84/DAY3.md)
📚 에피소드 6 ~ 에피소드 10
✔️ TIL
- 월 | Assignment #04
📚 복습
✔️ 🙋🏻 퀴즈 Quiz (1)
- 화 | [Assignment #05](https://gitlab.com/bonni-e/today-i-learned/-/blob/main/IT%205%EB%B6%84%20%EC%9E%A1%ED%95%99%EC%82%AC%EC%A0%84/DAY5.md)
📚 에피소드 11 ~ 에피소드 15
✔️ TIL
- 수 | [Assignment #06](https://gitlab.com/bonni-e/today-i-learned/-/blob/main/IT%205%EB%B6%84%20%EC%9E%A1%ED%95%99%EC%82%AC%EC%A0%84/DAY6.md)
📚 에피소드 16 ~ 에피소드 21
✔️ TIL
- 목 | [Assignment #07](https://gitlab.com/bonni-e/today-i-learned/-/blob/main/IT%205%EB%B6%84%20%EC%9E%A1%ED%95%99%EC%82%AC%EC%A0%84/DAY7.md)
📚 에피소드 22 ~ 에피소드 25
✔️ TIL
---
#### 2 주차
- 금 | Assignment #08
📚 복습
✔️ 🙋🏻 퀴즈 Quiz (2)
- 토 | [Assignment #09](https://gitlab.com/bonni-e/today-i-learned/-/blob/main/IT%205%EB%B6%84%20%EC%9E%A1%ED%95%99%EC%82%AC%EC%A0%84/DAY9.md)
📚 에피소드 26 ~ 에피소드 29
✔️ TIL
- 일 | [Assignment #10](https://gitlab.com/bonni-e/today-i-learned/-/blob/main/IT%205%EB%B6%84%20%EC%9E%A1%ED%95%99%EC%82%AC%EC%A0%84/DAY10.md)
📚 에피소드 30 ~ 에피소드 34
✔️ TIL
- 월 | Assignment #11
📚 복습
✔️ 🔥 미션 Mission (1)
- 화 | [Assignment #12](https://gitlab.com/bonni-e/today-i-learned/-/blob/main/IT%205%EB%B6%84%20%EC%9E%A1%ED%95%99%EC%82%AC%EC%A0%84/DAY11.md)
📚 에피소드 35 ~ 에피소드 38
✔️ TIL
- 수 | [Assignment #13](https://gitlab.com/bonni-e/today-i-learned/-/blob/main/IT%205%EB%B6%84%20%EC%9E%A1%ED%95%99%EC%82%AC%EC%A0%84/DAY13.md)
📚 에피소드 39 ~ 에피소드 45
✔️ TIL
- 목 | [Assignment #14](https://gitlab.com/bonni-e/today-i-learned/-/blob/main/IT%205%EB%B6%84%20%EC%9E%A1%ED%95%99%EC%82%AC%EC%A0%84/DAY14.md)
📚 복습
✔️ 🔥 미션 Final Mission (2)
---
