### DAY 9 (2023.01.21)

**🔖 오늘 읽은 범위 : 에피소드 26 ~ 에피소드 29**

---
😃 **책에서 기억하고 싶은 내용을 써보세요.**

- 클린 코드 (clean code)
    - 클린코드 : 설명이 필요 없는 코드 (이해가 되어지는 코드)
    - Tip 5.
        - 적절한 이름 짓기 (변수, 함수 등)
        - 함수명은 동사로 짓기 (기능, 액션은 하나)
        - 매개 변수의 개수는 적게하기 (3개 이하)
        - 불린 값(boolean type)을 인자로 받아 분기를 나누지 않기 (두 개 기능 처리가 됨)
        - (나만 아는) 축약어 쓰지 않기

- **[정렬 알고리즘](https://www.geeksforgeeks.org/insertion-sort/?ref=lbp)** (sorting)
    - **_시간 복잡도가 같은데 성능(속도)이 차이나는 이유는?_**
        - 모든 알고리즘은 초기 데이터 상태에 따라서도 속도가 달라질 수 있음 
        - 평균적으로 보다 빠른(효율적인) 알고리즘이 존재할 수 있는 것 

    1. **버블 정렬** (bubble sort) : O(N²), 값 교환 사이클 多 → bad
        - 짝꿍 값과 비교  
        ```
        // bubble sort
        int[] arr = { 8, 1, 2, 6, 3, 4, 9, 5, 7 };

        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = 0; j < arr.length - 1 - i; j++) {
                if (arr[j] > arr[j + 1]) {
                    int temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
            }
        }
        System.out.println(Arrays.toString(arr));
        ```
    2. **선택 정렬** (selection sort) : O(N²), 값 교환 사이클 少 → good
        - 최소 또는 최대값의 위치를 선택 기억 
        ```
        // selection sort
        int[] arr = { 8, 1, 2, 6, 3, 4, 9, 5, 7 };

        for (int i = 0; i < arr.length - 1; i++) {
            int min = arr[i];
            int minIdx = i;
            for (int j = i + 1; j < arr.length; j++) {
                if (min > arr[j]) {
                    min = arr[j];
                    minIdx = j;
                }
            }
            int temp = arr[i];
            arr[i] = arr[minIdx];
            arr[minIdx] = temp;
        }
        System.out.println(Arrays.toString(arr));
        ```
        
    3. **삽입 정렬** (insertion sort) : O(N²), 값 교환 X 밀어넣기 사이클 O → best 
        - 왼쪽을 바라보며(1인데스부터 시작) 모든 값 비교 
        ```
        // insertion sort
        int[] arr = { 8, 1, 2, 6, 3, 4, 9, 5, 7 };

        for (int i = 1; i < arr.length; i++) {
            int key = arr[i];
            int j = i - 1;

            while (j >= 0 && arr[j] > key) {
                arr[j + 1] = arr[j];
                j --;
            }
            arr[j + 1] = key;
        }
        System.out.println(Arrays.toString(arr));
        ```
    4. _**3가지 정렬 알고리즘의 비교**_ 
        - 직접 해봤습니다 👾
    - ![sort](img/sort.jpg){: width="800"}
- **스택과 큐** (_추상 자료구조_ abstract data type = ADT)
    - **stack**
    - ![stack](img/stack.png){: width="300"}
        - 위로 데이터를 쌓아 올리는 형식 
        - 위에서부터 데이터를 꺼낼 수 있음
        - LIFO (Last in, firt out)
        - ex) 
            - 웹 브라우저 뒤로가기 
            - 되돌리기 (ctrl+z)
    - **queue**
    - ![](https://thumbs.gfycat.com/DisguisedAromaticAxolotl-mobile.mp4)
        - 뒤로 줄을 세우는 형식
        - 맨 앞에서부터 데이터를 꺼낼 수 있음 
        - FIFO (First in, first out)
        - ex)
            - 쇼핑몰 주문 처리 시스템 
- **해시 테이블**
    - **[hashing](https://craftinginterpreters.com/hash-tables.html)** (자료를 검색하기 위한 자료 구조)
    - ![hash](https://upload.wikimedia.org/wikipedia/commons/thumb/7/7d/Hash_table_3_1_1_0_1_0_0_SP.svg/1200px-Hash_table_3_1_1_0_1_0_0_SP.svg.png){: width="400"}
    - ![hash](img/hash.png){: width="600"}
        - key-value 짝지어 모은 것
        - 선형 검색과의 시간 복잡도 차이 
            - 선형 검색 : O(N)
            - 해시 테이블 : O(1) + α(해시 충돌 hash collision 대처 시간) 👈🏻 일반은 무려 상수시간(constant time) 만큼만 소요됨 
        - 해시 테이블 = 일반 배열 + 해시 함수(속도의 비결)
        - 해시 함수의 역할 : 키 값을 인덱스로 변환함 

🤔 **오늘 읽은 소감은? 떠오르는 생각을 가볍게 적어보세요**

```

```

🔎 **궁금한 내용이 있거나, 잘 이해되지 않는 내용이 있다면 적어보세요.**

- 
-
