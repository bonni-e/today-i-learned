### DAY 6 (2023.01.18)

**🔖 오늘 읽은 범위 : 에피소드 16 ~ 에피소드 21**

---
😃 **책에서 기억하고 싶은 내용을 써보세요.**

- 브라우저 엔진 (= 렌더링 엔진)
    - 프론트엔드(front-end) 기술 지원 
        - 파이어폭스 : 게코
        - 사파리 : 웹킷
        - 크롬, 엣지 : 블링크 
- 인터넷 익스플로러 사망
    - Ajax 최초 지원(새로고침 없이 업데이트 데이터 표기)할만큼 최신 기술을 지원했었음 
    - 사망 원인 : 점유율만 믿고 게으름을 피움 (수 많은 보안 취약점도 발생) 
- HTTP 쿠키 (cookie)
    - 브라우저를 통해 사용자 PC에 보관하는 기록물 
    - HTTP(HyperText Transfer Protocol)
        - stateless (서버는 항상 연결되어 있지 않음)
    - 쿠키 사용 규칙 
        - 도메인 1개에 한정함 
        - 자동으로 보냄 (PC -> 서버)
        - 자동으로 저장 (서버-> PC)
- 서버 (server) = 컴퓨터
    - 365일 24시간 인터넷 연결 및 작동중
    - 주소(IP)를 입력 받으면, 요청한 페이지(데이터)를 서버가 보여줌 
- 풀스택 (fullstack) 
    - = frontend + backend + DevOps (development & Operations)
    - 풀스택 개발자의 업무영역이 '모든 일'에 해당하지 않음으로, 반드시 업무 경계를 확인할 것 
        - 백엔드로 노드제이에스를 사용하나요, 파이썬을 사용하나요?
        - 데브옵스를 할 수는 있지만 개발 업무에 더 집중하고 싶은데, 개발 프로세스가 어떻게 되나요?
        - 팀원의 수와 담당 포지션은 어떻게 되나요?
- 서버리스 (serverless)
    - 수동으로 직접 관리하는 서버 실체가 없음 (대여)
        - 각종 사고를 관리해주는 클라우드 서비스 등장 (아마존 EC2 등)
    - 애플리케이션을 함수 단위로 나누어 -> 업로드 -> 요청이 있을때만 깨어남 
        - 장점 : 고효율, 저비용 
        - 단점 : 반응이 느림(cold start 시간이 필요함), 서버 제공자에 대한 의존도 높음
    - 상품 : [AWS Lamda](https://aws.amazon.com/ko/lambda/?nc2=h_ql_prod_fs_lbd), [Google Cloud Funtions](https://cloud.google.com/functions), [Apex](https://www.dell.com/ko-kr/dt/apex/cloud-services/index.htm), [Terraform](https://www.terraform.io/) 등  

🤔 **오늘 읽은 소감은? 떠오르는 생각을 가볍게 적어보세요**

```
사이드 프로젝트를 서버리스로 구현해봐야겠다. 
AWS S3로도 함수 쪼개기가 가능하다고 들었었는데, 시도해봐야겠다.
```

🔎 **궁금한 내용이 있거나, 잘 이해되지 않는 내용이 있다면 적어보세요.**

- 
-

**🗂 오늘의 과제 :  나의 최애 TIL 선정**

- [Kinn님의 TIL](https://feline-aspen-138.notion.site/742030a8a27a405582d9e93018bd7b8d?v=86d5c5fe8f6e44d287571eba9e7b7e9d)
-
